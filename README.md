# DDOT Transit Feed Generators #

![DC Circulator National Mall Route bus](http://dccirculator.wpengine.com/wp-content/uploads/2015/07/Jefferson_Memorial_NoBarriers_2400x1602_sm-e1439996640127.jpg)

__This repository contains a folder each for the Streetcar and the Circulator, which in turn contain scripts. These take the District Department of Transportation's GIS Circulator and Streetcar line and stop geometries and convert them into GTFS data.__
## Summary of Contents ##

__GenerateStopOrder\_STEP1.py__ reads the polyline and point shapefiles stored on DDOT's GIS servers, creates local copies in a scratch geodatabase (__scratch.gdb__�the script creates it if it doesn't exist) which is ignored in git and only stored locally. The script then uses ArcPy to generate linear referencing system (LRS) routes and the order of the bus/streetcar stops.

__GenerateGTFSfromGIS\_Step2.py__ connects to the scratch geodatabase and using the GIS geometries and attributes generates the transit feed in the form of a GTFS ZIP archive.

__CheckingRoutesStops.mxd__ and __GTFS.mxd__ are ArcMap documents accessing shapefiles in the scratch.gdb.

## How do I get set up? ##

The scripts are written in Python 2.7. You must have access to the modules imported by the scripts, including the ArcPy package. To run _GenerateStopOrder.py_ you need access to DDOT's GIS using the connection name 'Connection to DDOTGIS as PTSA.sde'. Connections can be issued through DDOT OITI. If you are otherwise connected to DDOT GIS and your connection includes access to PTSA.DCCirculatorLn and PTSA.DCCirculatorPt, make sure you get the connection with the correct name or, alternatively, locally modify the connection name in _GenerateStopOrder.py_ lines 31 to 37.

### Run instructions ###
1. Run _GenerateStopOrder\_Step1.py_.
2. Verify that the scratch database has been correctly created and populated through the mapfile _CheckingRoutesStops.mxd_. 
3. Run _GenerateGTFSfromGIS\_Step2.py_.
4. Review the console output to make sure the script executed properly and for troubleshooting purposes.

### When do I run the scripts? ###

The scripts should be run for regular feed production, yearly or seasonally. Both script should be run in order if there is any change to the original GIS layer that is relevant to GTFS production, e.g.:.
+ Adding a stop or line attribute, such as regional stop ID
+ A long-term detour
+ Stop temporarily closed for reconstruction or otherwise

### When do I run the scripts for regular production? ###

#### Streetcar ####
As of 2017, the DC Streetcar does not have different summer and winter schedules. As such, you can run the scripts at any time of the year. When you produce a Streetcar feed, it is valid for the current year and the first six months of the next year. For example, if you run the script in November 2017, the resulting feed will be valid for 2017 in its entirety and until June 2018. This being said, we recommend you produce a new Streetcar feed as early as possible during the new year, for example in __January__ or early __February__, so that the holiday exceptions are properly implemented.

#### Circulator ####
Because certain Circulator route schedules vary by season (see details [here](http://www.dccirculator.com/ride/rider-tools/schedule/)), the scripts have to be run at least twice a year, once in the winter (for the coming summer schedule to be included) and once in the summer (for the coming winter schedule to be included). When you run the script before or after July 1st the resulting feed changes. 

> For your reference, though it is in no way necessary to know this, when you run the second Circulator script:

> + _Before July 1st, i.e. Jan-Jun of Year Y, inclusive:_ Resulting feed is valid only from 1 Jan Y to End of Summer Schedule[�](#markdown-header-footnotes) Y (~30 September Y).
> + _On or after July 1st, i.e. Jul-Dec of Year Y, inclusive:_ Resulting feed is valid from 1 Oct Y to End of Winter Schedule[�](#markdown-header-footnotes) in the next year (~31 March Y+1).

With this in mind, it is recommended to run the Circulator generator once in __January__ and once in __July__, so  that the schedules are updated well in advance and the holiday exceptions reflected in the feed.

Update the schedule for Washington National home game, find the schdule with date and start time and saved in csv file named as natsschedule_year (eg,natsschedule_2018)
then rerun the program.
## Who do I talk to? ###
* David Koch (david.koch@dc.gov)
* James Graham (james.graham@dc.gov)
* Ehab Ebeid (ehab.ebeid@dc.gov / ee253@cornell.edu)

## Footnotes ##
�The last day of the Circulator summer schedule is the day (Saturday) before the first Sunday of October, and the last day of the winter schedule is the day before the first Sunday of April.