#-------------------------------------------------------------------------------
# Name:         Generate GTFS Stop Order
# Purpose:      This script takes the existing GIS bus polylines and creates LRS routes.  Also associates GIS
#               stop points to the LRS, giving them an order (depending on direction).
# Author:       jgraham
# Created:      27/05/2015
# Copyright:    (c) jgraham 2015
# Licence:      CC0 1.0 Universal
#-------------------------------------------------------------------------------

def main():
    '''This script takes the existing bus lines and creates routes.
    --circulator bus stops are LFAR'ed to the routes to get an approximated sequence for 'stop_times.txt', by LineID
    some stops may be used by more than one line, so it is important for the data owner to populate the LineID with a comma-delimited
    color for the corresponding lines, eg 'Orange,Blue' that the stop serves.  This gets inserted into the GTFS feed.
    '''
    def Table2Dict(inputFeature,key_field,field_names,filter=None):
        """
        Converts table to nested dictionary.
        The key_field -- feature ID for input feature class
        field_names -- selected field names for input feature class
        return outDict={key_field:{field_name1:value1,field_name2:value2...}}
        """
        outDict={}
        field_names.insert(0,key_field)
        with arcpy.da.SearchCursor(inputFeature,field_names,where_clause=filter) as cursor:
            for row in cursor:
                outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
        return outDict
    
    def xstr(s):
        if s:
            str(s)
        else:
            return ""

    import arcpy
    import sys, os
    
    print 'Initializing...'
    
    scriptloc = sys.path[0]
    localdb = os.path.join(scriptloc,'scratch.gdb')
    #workspace =r"Database Connections\Connection to DDOTGIS as DDOTEditor.sde"
    arcpy.env.workspace = localdb
    arcpy.env.overwriteOutput = True

    #data
    currentstops = os.path.join(scriptloc,"Connection to DDOTGIS as PTSA.sde","PTSA.DCCirculatorPt")
    currentlines = os.path.join(scriptloc,"Connection to DDOTGIS as PTSA.sde","PTSA.DCCirculatorLn")
    locallines = os.path.join(localdb,'circulatorlines')
    localstops = os.path.join(localdb,'circulatorstops')
    localroutes = os.path.join(localdb,'circulatorroutes')
    stopevents = os.path.join(localdb,'circulatorstopevents')
    stopevents_temp = stopevents + "_temp"

    # Create the master FileGeodatabase as needed.
    if not (arcpy.Exists(localdb)):
        print os.path.basename(localdb) + " doesn't exist; creating it now..."
        arcpy.CreateFileGDB_management(scriptloc, os.path.basename(localdb))

    if not (arcpy.Exists(stopevents)):
        # Also create stop events table, for all appended events.
        arcpy.CreateTable_management(localdb, os.path.basename(stopevents))
        arcpy.AddField_management(stopevents,"route","TEXT",field_length=250)
        arcpy.AddField_management(stopevents,"MEAS","FLOAT",field_precision=8, field_scale=3)
        arcpy.AddField_management(stopevents,"Distance","FLOAT",field_precision=8, field_scale=3)
        arcpy.AddField_management(stopevents,"INPUTOID","LONG")
        arcpy.AddField_management(stopevents,"STOPID","TEXT",field_length=50)
        arcpy.AddField_management(stopevents,"STOPORDER","LONG")
        arcpy.AddField_management(stopevents,"ROUTESTOPID","TEXT",field_length=250)
        
    # Make local copy of the lines and points.
    arcpy.CopyFeatures_management(currentlines,locallines)
    arcpy.CopyFeatures_management(currentstops,localstops)

    where_clause = "StopStatus = 'Active'"
    stopslayer = "stopslayer"
    arcpy.MakeFeatureLayer_management(localstops, stopslayer,where_clause=where_clause)

    RegBusIDs_dict = Table2Dict(stopslayer, "OID@",["REG_ID"])
    arcpy.AddField_management(locallines,"RouteID","TEXT",field_length=100)

    # Calculate RouteID.
    with arcpy.da.UpdateCursor(locallines, ['RouteID', 'LineID', 'DIRECTION', 'LINE_ALT']) as lcursor:
        for row in lcursor:
            if row[3]:
                row[0] = str(row[1])+ "-" + row[3]+ "_" +str(row[2])
            else:
                row[0] = str(row[1])+ "_" +str(row[2])
            lcursor.updateRow(row)
            print row ###


    arcpy.AddField_management(locallines,"FromM","Float",field_precision=8, field_scale=3)
    arcpy.CalculateField_management(locallines,"FromM",'0',expression_type='PYTHON_9.3')
    arcpy.AddField_management(locallines,"ToM","Float",field_precision=8, field_scale=3)
    arcpy.CalculateField_management(locallines,"ToM",'!Shape_Length!',expression_type='PYTHON_9.3')

    arcpy.MakeFeatureLayer_management(locallines, "lineslayer",where_clause = "LINE_STATUS = 'Active'")

    arcpy.CreateRoutes_lr("lineslayer","RouteID",localroutes,'TWO_FIELDS',
                        from_measure_field='FromM',
                        to_measure_field='ToM',
                        ignore_gaps=False,
                        build_index=True)

    with arcpy.da.SearchCursor(localroutes,['RouteID']) as routecursor:
        if arcpy.Exists(stopevents_temp):
            arcpy.TruncateTable_management(stopevents)

        for route in routecursor:
            if not arcpy.Exists(stopevents_temp):
                arcpy.CreateTable_management(localdb, 'circulatorstopevents_temp', stopevents)
            else:
                arcpy.TruncateTable_management(stopevents_temp)
            print route ###

            arcpy.MakeFeatureLayer_management(localroutes, "routeslayer",where_clause = "RouteID = '" + route[0] + "'")
            arcpy.LocateFeaturesAlongRoutes_lr(stopslayer,"routeslayer","RouteID","30 Feet",stopevents_temp,"route POINT MEAS",
                                route_locations="ALL",
                                distance_field="DISTANCE",
                                in_fields="NO_FIELDS")

            fieldmapping = """route "route" true true false 100 Text 0 0 ,First,#,{0},route,-1,-1;MEAS "MEAS" true true false 8 Double 0 0 ,First,#,{0},MEAS,-1,-1;Distance "Distance" true true false 8 Double 0 0 ,First,#,{0},Distance,-1,-1;INPUTOID "INPUTOID" true true false 4 Long 0 0 ,First,#,{0},INPUTOID,-1,-1;STOPORDER "STOPORDER" true true false 4 Long 0 0 ,First,#"""
            fieldmapping = fieldmapping.format(stopevents_temp)
            arcpy.Append_management(inputs=stopevents_temp,
                                    target=stopevents,
                                    schema_type="NO_TEST",
                                    field_mapping=fieldmapping)

    event_dict = dict()
    arcpy.AddField_management(stopevents,"STOPORDER","LONG")
    arcpy.AddField_management(stopevents,"ROUTESTOPID","TEXT")
    thisroute = ""
    previousroute = ""
    thisstop = 0
    
    with arcpy.da.UpdateCursor(stopevents,['route', 'MEAS','INPUTOID','OID@','STOPORDER','ROUTESTOPID','STOPID'],sql_clause=(None, 'ORDER BY route, MEAS')) as eventcursor:
        for event in eventcursor:
            
            thisroute = event[0]            
            thisid = RegBusIDs_dict[event[2]]['REG_ID']
            
            if thisid and thisid != '' and thisid != ' ':
                stopid = str(thisid)
            else:
                stopid = 'DDOT-' + str(event[2])

            event[6] = stopid
            event[5] = thisroute + "_" + stopid
            
            if previousroute == "":
                event[4] = thisstop
                
            elif thisroute == previousroute:
                #same route, increment
                thisstop += 1
                event[4] = thisstop

            else:
                #new route
                thisstop = 0
                event[4] = thisstop

            eventcursor.updateRow(event)

            event_dict[stopid] = event[1]

            previousroute = thisroute
            print thisroute

    # Begin processing stop events.
    # Work by line/route and query for matches in event table.
    # Exclude event records that are not active.
    # Exclude event records that don't match the line id (e.g. if I'm working on a red line, only search for stops on red).
    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(localdb)

    # Edit session is started without an undo/redo stack for versioned data.
    # (For second argument, use False for unversioned data.)
    edit.startEditing(False, False)

    # Start an edit operation.
    edit.startOperation()

    # First, populate the MEAS value in the STOPS table.
    with arcpy.da.UpdateCursor(localstops,['LINE','LineID','STOPSTATUS','StopOrder','OID@', 'STOPMEAS','STOPERROR']) as stopcursor1:
        for stop in stopcursor1:
            if stop[4] in event_dict:
                thismeas = event_dict[stop[4]]
                stop[5] = thismeas
            else:
                # This stop wasn't found, indicate error in STOPERROR.
                stop[6] = ''
                stop[5] = None

            stopcursor1.updateRow(stop)
            print stop ###

    colors = [] # list of line colors we've visited.  If it's in here already, we skip it.

    # Then, update the Stop Order value on the stops FC:
    with arcpy.da.SearchCursor(localroutes,['RouteID']) as cursor:
        for row in cursor:

            thisroute = row[0]
            thiscolor = thisroute.split("_")[0]
            if thiscolor in colors:
                continue

            thisindex = 0
            where_clause="STOPSTATUS = 'Active' AND STOPMEAS IS NOT NULL AND LOWER(LineID) in('" + thiscolor.lower() + "')"

            with arcpy.da.UpdateCursor(localstops,['LINE','LineID','STOPSTATUS','StopOrder','OID@', 'STOPMEAS'],where_clause=where_clause, sql_clause=(None, 'ORDER BY STOPMEAS')) as stopcursor2:
                for stop in stopcursor2:
                    thismeas = event_dict[stop[4]]
                    stop[3] = thisindex
                    thisindex+=1
                    print thisroute, str(thisindex)
                    stopcursor2.updateRow(stop)

            colors.append(thiscolor)
            print row ###

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes.
    edit.stopEditing(True)

    print "Yay, I'm done!"

if __name__ == '__main__':
    main()
