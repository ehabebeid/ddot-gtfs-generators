#!/usr/bin/env python

# -------------------------------------------------------------------------------
# Name:        Generate GTFS from GIS
# Author:      jgraham
# Created:     27/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     CC0 1.0 Universal
# -------------------------------------------------------------------------------

"""
DDOT's Circulator to GIS conversion script
This script takes DDOT's production DC Circulator stops and linework and converts it into a complete GTFS feed.
"""

from zipfile import ZipFile
import sys, os
import arcpy
import csv
from cStringIO import StringIO
import collections
from collections import defaultdict
import datetime
from datetime import timedelta
import shutil


def Table2Dict(inputFeature,key_field,field_names,filter=None):
    """
    Converts table to nested dictionary.
    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names,where_clause=filter) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

def getWinterSummerFirstSundays(thisyear):
    """
    Creates a 4-item dict that is the GTFS calendar date format for the first Sunday of April and October.
    """

    # Code reference:
    # http://stackoverflow.com/questions/2003870/how-can-i-select-all-of-the-sundays-for-a-year-using-python

    d = datetime.date(thisyear, 1, 1)                    # January 1st
    d += datetime.timedelta(days = 6 - d.weekday())      # First Sunday
    foundSpring = False
    foundFall = False
    GTFSdates = dict()

    while d.year == thisyear:
        d += datetime.timedelta(days = 7)
        # first
        if not foundSpring:
            if d.month == 4:
                # first occurrence of this Sunday in April
                seasonchange = d
                foundSpring = True
                year, month, day = str(thisyear), str(seasonchange.month), str(seasonchange.day)
                GTFSdates["summerDateStart"] = year + month.zfill(2) + day.zfill(2)

                onedayback = seasonchange - datetime.timedelta(days=1)
                year, month, day = str(thisyear), str(onedayback.month), str(onedayback.day)
                GTFSdates["winterDateEnd"] = year + month.zfill(2) + day.zfill(2)

        if not foundFall:
            if d.month == 10:
                # first occurrence of this Sunday in April
                seasonchange = d
                foundFall = True
                year, month, day = str(thisyear), str(seasonchange.month), str(seasonchange.day)
                GTFSdates["winterDateStart"] = year + month.zfill(2) + day.zfill(2)

                onedayback = seasonchange - datetime.timedelta(days=1)
                year, month, day = str(thisyear), str(onedayback.month), str(onedayback.day)
                GTFSdates["summerDateEnd"] = year + month.zfill(2) + day.zfill(2)

    return GTFSdates

def getMinutesFromDistance(thismeasure,prevmeasure):
    """
    Take the from M-value and to M-value and derive a total number of minutes that
    it would take to traverse that segment, given a constant speed of 9 mph.
    """
    ### IN FUTURE: build a dict (key: route) of dicts (key: direction) with the average speed on those segments.
    ### Decide if those speeds will be trip-wide averages or averages of speeds between stops.
    
    differenceInMeters = thismeasure - prevmeasure
    differenceInMiles = differenceInMeters*0.000621371
    return (differenceInMiles/9)*60 # Time it takes to drive distance at 9 MPH in minutes.

def convertDomainTime(domainIndex, conversiontype='getgtfstimestring'):
    """
    Accepts three string values in the conversiontype parameter: 'getgtfstimestring', 'getdomaindatetime', 'getdomainday'.
    Uses one of three dictionary lookups, depending on conversion type, to return an datetime part or string-formatted datetime part.
    """

    if conversiontype == 'getgtfstimestring':
        domaintogtfsstring = {1: '04:00:00', 2: '04:30:00', 3: '05:00:00', 4: '05:30:00', 5: '06:00:00', 6: '06:30:00',
                   7: '07:00:00', 8: '07:30:00', 9: '08:00:00', 10: '08:30:00',
                   11: '09:00:00', 12: '09:30:00', 13: '10:00:00', 14: '10:30:00', 15: '11:00:00', 16: '11:30:00',
                   17: '12:00:00', 18: '12:30:00', 19: '13:00:00', 20: '13:30:00',
                   21: '14:00:00', 22: '14:30:00', 23: '15:00:00', 24: '15:30:00', 25: '16:00:00', 26: '16:30:00',
                   27: '17:00:00', 28: '17:30:00', 29: '18:00:00', 30: '18:30:00',
                   31: '19:00:00', 32: '19:30:00', 33: '20:00:00', 34: '20:30:00', 35: '21:00:00', 36: '21:30:00',
                   37: '22:00:00', 38: '22:30:00', 39: '23:00:00', 40: '23:30:00',
                   41: '24:00:00', 42: '24:30:00', 43: '25:00:00', 44: '25:30:00', 45: '26:00:00', 46: '26:30:00',
                   47: '27:00:00', 48: '27:30:00'}
        return domaintogtfsstring[domainIndex]

    elif conversiontype == 'getdomaindatetime':
        domaintodatetime = {1: datetime.datetime(2015, 1, 1, 4, 0, 0), 2: datetime.datetime(2015, 1, 1, 4, 30, 0), 3: datetime.datetime(2015, 1, 1, 5, 0, 0), 4: datetime.datetime(2015, 1, 1, 5, 30, 0), 5: datetime.datetime(2015, 1, 1, 6, 0, 0), 6: datetime.datetime(2015, 1, 1, 6, 30, 0),
                   7: datetime.datetime(2015, 1, 1, 7, 0, 0), 8: datetime.datetime(2015, 1, 1, 7, 30, 0), 9: datetime.datetime(2015, 1, 1, 8, 0, 0), 10: datetime.datetime(2015, 1, 1, 8, 30, 0),
                   11: datetime.datetime(2015, 1, 1, 9, 0, 0), 12: datetime.datetime(2015, 1, 1, 9, 30, 0), 13: datetime.datetime(2015, 1, 1, 10, 0, 0), 14: datetime.datetime(2015, 1, 1, 10, 30, 0), 15: datetime.datetime(2015, 1, 1, 11, 0, 0), 16: datetime.datetime(2015, 1, 1, 11, 30, 0),
                   17: datetime.datetime(2015, 1, 1, 12, 0, 0), 18: datetime.datetime(2015, 1, 1, 12, 30, 0), 19: datetime.datetime(2015, 1, 1, 13, 0, 0), 20: datetime.datetime(2015, 1, 1, 13, 30, 0),
                   21: datetime.datetime(2015, 1, 1, 14, 0, 0), 22: datetime.datetime(2015, 1, 1, 14, 30, 0), 23: datetime.datetime(2015, 1, 1, 15, 0, 0), 24: datetime.datetime(2015, 1, 1, 15, 30, 0), 25: datetime.datetime(2015, 1, 1, 16, 0, 0), 26: datetime.datetime(2015, 1, 1, 16, 30, 0),
                   27: datetime.datetime(2015, 1, 1, 17, 0, 0), 28: datetime.datetime(2015, 1, 1, 17, 30, 0), 29: datetime.datetime(2015, 1, 1, 18, 0, 0), 30: datetime.datetime(2015, 1, 1, 18, 30, 0),
                   31: datetime.datetime(2015, 1, 1, 19, 0, 0), 32: datetime.datetime(2015, 1, 1, 19, 30, 0), 33: datetime.datetime(2015, 1, 1, 20, 0, 0), 34: datetime.datetime(2015, 1, 1, 20, 30, 0), 35: datetime.datetime(2015, 1, 1, 21, 0, 0), 36: datetime.datetime(2015, 1, 1, 21, 30, 0),
                   37: datetime.datetime(2015, 1, 1, 22, 0, 0), 38: datetime.datetime(2015, 1, 1, 22, 30, 0), 39: datetime.datetime(2015, 1, 1, 23, 0, 0), 40: datetime.datetime(2015, 1, 1, 23, 30, 0),
                   41: datetime.datetime(2015, 1, 2, 0, 0, 0), 42: datetime.datetime(2015, 1, 2, 0, 30, 0), 43: datetime.datetime(2015, 1, 2, 1, 0, 0), 44: datetime.datetime(2015, 1, 2, 1, 30, 0), 45: datetime.datetime(2015, 1, 2, 2, 0, 0), 46: datetime.datetime(2015, 1, 2, 2, 30, 0),
                   47: datetime.datetime(2015, 1, 2, 3, 0, 0), 48: datetime.datetime(2015, 1, 2, 3, 30, 0)}
        return domaintodatetime[domainIndex]

    elif conversiontype == 'getdomainday':
        domaintoday = {1:'Monday',
                    2:'Tuesday',
                    3:'Wednesday',
                    4:'Thursday',
                    5:'Friday',
                    6:'Saturday',
                    7:'Sunday'}
        return domaintoday[domainIndex]

def sec(h, m, s=0):
    return (((h * 60) + m) * 60) + s

def tstr(h, m, s=0):
    # Handle (possibly negative) minute average.
    s = sec(h, m, s)
    m = s / 60
    s = s % 60
    h = m / 60
    m = m % 60
    return '%02d:%02d:%02d' % (h, m, s)

def generateCalendarText(thisserviceid, servicedaystart, servicedayend,servicedesc):
    """
    Returns a string, coded for this calendar year corresponding to the GTFS calendar.txt.
    example:
    service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date
    WE,0,0,0,0,0,1,1,20060701,20060731
    WD,1,1,1,1,1,0,0,20060701,20060731
    """
    today = datetime.datetime.now()
    fromdate = str(today.year) + '0101'
    todate = str(today.year) + '1231'
    firstsundays = getWinterSummerFirstSundays(today.year)
    
    if servicedesc:
        if 'summer' not in servicedesc.lower() and 'nats' not in servicedesc.lower():
            if today.month > 6: # Ehab 20170814
                # This is after June, so create end of year winter dates.
                fromdate = firstsundays['winterDateStart']
                todate = getWinterSummerFirstSundays(today.year+1)['winterDateEnd'] # Ehab 20170814
            else:
                # This is before June, so create start of year winter dates.
                todate = firstsundays['winterDateEnd']

        if 'summer' in servicedesc.lower():
            fromdate = firstsundays['summerDateStart']
            todate = firstsundays['summerDateEnd']

    calendarlist = [thisserviceid,0,0,0,0,0,0,0,fromdate,todate]

    if 'nats' not in thisserviceid.lower():
        if servicedaystart != 7:
            for i in range(servicedaystart,servicedayend+1):
                calendarlist[i] = 1
        else:
            # sunday start
            if servicedayend != 7:
                servicedaystart = 1
                for i in range(servicedaystart,servicedayend+1):
                    calendarlist[i] = 1
    
            calendarlist[7] = 1 # Update the Sunday column separately.

    return calendarlist

def main():
    # These specify some GTFS values for ADA, Bike, etc, for your reference.
    WHEELCHAIR_YES = 1
    WHEELCHAIR_NO = 2
    WHEELCHAIR_UNDEF = 0
    BIKE_YES = 2
    BIKE_NO = 1
    BIKE_UNDEF = 0
    LOCATION_STOP = 0
    LOCATION_STATION = 1
    PICKDROP_SCHEDULED = 0
    PICKDROP_NO = 1
    PICKDROP_PHONE = 2
    PICKDROP_COORDINATE = 3
    #
    
    print 'Initializing...'
    
    scriptloc = sys.path[0] # Local path
    arcpy.env.overwriteOutput = True
    localdb = os.path.join(scriptloc, 'scratch.gdb') # Scratch geodatabase

    # GTFS output...
    gtfs = ZipFile(os.path.join(scriptloc, 'dc-circulator.zip'), mode='w')

    # Local data...
    locallines = os.path.join(localdb, 'circulatorlines')
    localroutes = os.path.join(localdb, 'circulatorroutes')
    localstops = os.path.join(localdb, 'circulatorstops')
    WGSlines = os.path.join(localdb, "circulatorlines_WGS")
    WGSstops = os.path.join(localdb, "circulatorstops_WGS")

    # Set environment to WGS84 standard and coordinate system.
    arcpy.env.outputCoordinateSystem = arcpy.SpatialReference("WGS 1984")
    arcpy.env.geographicTransformations = "NAD_1983_To_WGS_1984_1"

    # Make local copy of the lines and stops as WGS84.
    arcpy.CopyFeatures_management(locallines, WGSlines)
    arcpy.CopyFeatures_management(localstops, WGSstops)

    stopevents = os.path.join(localdb, "circulatorstopevents")

    # Use search cursor on lines shapefile to build service times dict.
    # This will be used to reference while building trips below and also for building the calendar.txt.
    servicetimes = dict()
    with arcpy.da.SearchCursor(WGSlines, ['LineID','ServiceDayDescription_A', 'ServiceHourStart_A', 'ServiceHourEnd_A', 'ServiceDayStart_A', 'ServiceDayEnd_A',
                                            'ServiceDayDescription_B', 'ServiceHourStart_B', 'ServiceHourEnd_B', 'ServiceDayStart_B', 'ServiceDayEnd_B',
                                            'ServiceDayDescription_C', 'ServiceHourStart_C', 'ServiceHourEnd_C', 'ServiceDayStart_C', 'ServiceDayEnd_C',
                                            'ServiceDayDescription_D', 'ServiceHourStart_D', 'ServiceHourEnd_D', 'ServiceDayStart_D', 'ServiceDayEnd_D',
                                            'ServiceDayDescription_E', 'ServiceHourStart_E', 'ServiceHourEnd_E', 'ServiceDayStart_E', 'ServiceDayEnd_E',
                                            'LINE_ALT'], where_clause="LINE_STATUS = 'Active'") as cursor:
        for line in cursor:
            thisid = ""
            if line[26]:
                # This is an alternate route.  Build in alt text to the service id.
                thisid = line[0] + "-" + line[26] # Here, I'm using a hyphen to keep the line-alt info intact so that the line shape can be associated with this schedule info.  The delimiter will be underscores below.
            else:
                thisid = line[0]
            # Service dict
            servicetimes[thisid] = {'ServiceDayDescription_A':line[1], 'ServiceHourStart_A':line[2], 'ServiceHourEnd_A':line[3], 'ServiceDayStart_A':line[4], 'ServiceDayEnd_A':line[5],
                                        'ServiceDayDescription_B':line[6], 'ServiceHourStart_B':line[7], 'ServiceHourEnd_B':line[8], 'ServiceDayStart_B':line[9], 'ServiceDayEnd_B':line[10],
                                        'ServiceDayDescription_C':line[11], 'ServiceHourStart_C':line[12], 'ServiceHourEnd_C':line[13], 'ServiceDayStart_C':line[14], 'ServiceDayEnd_C':line[15],
                                        'ServiceDayDescription_D':line[16], 'ServiceHourStart_D':line[17], 'ServiceHourEnd_D':line[18], 'ServiceDayStart_D':line[19], 'ServiceDayEnd_D':line[20],
                                        'ServiceDayDescription_E':line[21], 'ServiceHourStart_E':line[22], 'ServiceHourEnd_E':line[23], 'ServiceDayStart_E':line[24], 'ServiceDayEnd_E':line[25],
                                        'LINE_ALT':line[26]}

    calendar_file = StringIO()
    calendar_rows = csv.writer(calendar_file)
    servicegroups = ['A', 'B', 'C', 'D', 'E']
    servicegroupdata = dict()
    servicedays = dict()
    calendar_rows.writerow(['service_id','monday','tuesday','wednesday','thursday','friday','saturday','sunday','start_date','end_date'])
    for id,servicebands in servicetimes.items():
        # Here, I need to check each Service band (A, B,etc.) to pull out distinct service_id data to populate into the calendar file.
        # So, for example, if Blue_Monday_Friday, I need to populate the service row with:
        # Blue_Monday_Friday, 1,1,1,1,1,0,0,<date>,<dateto>

        for band in servicegroups:
            startid = servicetimes[id]['ServiceDayStart_' + band]
            endid = servicetimes[id]['ServiceDayEnd_' + band]
            timestartid = servicetimes[id]['ServiceHourStart_' + band]
            timeendid = servicetimes[id]['ServiceHourEnd_' + band]
            servicedescription = servicetimes[id]['ServiceDayDescription_' + band]

            if not startid or not endid:
                continue
            servicedaystart = convertDomainTime(startid,'getdomainday')
            servicedayend = convertDomainTime(endid,'getdomainday')

            # first, define the service_id
            thisserviceid = ""
            if (not servicetimes[id]['LINE_ALT'] or servicetimes[id]['LINE_ALT'] == ''):
                if (not servicedescription or servicedescription == ''):
                    # Service description is blank so don't use it.
                    thisserviceid = id + "_" + servicedaystart + "_" + servicedayend
                else:
                    # Some sort of service description is here (like night, summer, winter, etc)
                    thisserviceid = id + "_" + servicedescription + "_" + servicedaystart + "_" + servicedayend
            else:
                # Line_Alt present
                thisserviceid = id + "_" + servicedaystart + "_" + servicedayend

            print 'Service ID: ' + thisserviceid
            # calendarlist = [thisserviceid,0,0,0,0,0,0,0,fromdate,todate]
            calendarlistline = generateCalendarText(thisserviceid, startid, endid, servicedescription)
            calendar_rows.writerow(calendarlistline)
            servicedays[thisserviceid] = {'monday':calendarlistline[1],'tuesday':calendarlistline[2],'wednesday':calendarlistline[3],'thursday':calendarlistline[4],'friday':calendarlistline[5],'saturday':calendarlistline[6],'sunday':calendarlistline[7]}
            servicegroupdata[thisserviceid] = {"daystart":startid,"dayend":endid,"timestart":timestartid,"timeend":timeendid}

    gtfs.writestr('calendar.txt', calendar_file.getvalue())
    print 'Wrote calendar.txt.'

    print 'Starting to write calendar_dates...'

    calendar_dates_file = StringIO()
    calendar_dates = csv.writer(calendar_dates_file)
    header = ('service_id','date','exception_type') # calendar_dates file header
    calendar_dates.writerow(header)

    today = datetime.date.today() # Today's date
    thisyear = today.year

    # Fourth of July
    July4Date = datetime.date(thisyear,7,4)
    July4WD_ISO = July4Date.isoweekday() # ISO 8601 weekday, where Monday is 1 and Sunday is 7.
    July4WD_Name = convertDomainTime(July4WD_ISO, conversiontype='getdomainday') # Convert weekday ISO number to name.
    cd_July4Date = str(thisyear)+'0704'
    for serviceid in servicedays:
        if 'red' in serviceid.lower():
            # NM service
            if 'winter' not in serviceid.lower():
                # not winter service; i.e. potentially applicable
                if servicedays[serviceid][July4WD_Name.lower()] == 1:
                    # service is active on that day of the week
                    row = (serviceid,cd_July4Date,2)    # Then disable it.
                    calendar_dates.writerow(row)        # And write that row.
                    print "Fourth of July: on a "+July4WD_Name+"; disabled applicable NM service..."
                    print '                 ',row       # And print it.

    # Christmas Day
    XmasDate = datetime.date(thisyear,12,25)
    XmasWD_ISO = XmasDate.isoweekday() # ISO 8601 weekday, where Monday is 1 and Sunday is 7.
    XmasWD_Name = convertDomainTime(XmasWD_ISO, conversiontype='getdomainday') # Convert weekday ISO number to name.
    cd_XmasDate = str(thisyear)+'1225'
    print "Christmas Day: on a "+XmasWD_Name+"; disabling all applicable services..."
    for serviceid in servicedays:
        if 'summer' not in serviceid.lower() and 'nats' not in serviceid.lower():
        # This service is not a summer or Nats service, i.e. it is potentially applicable.
            if servicedays[serviceid][XmasWD_Name.lower()] == 1:
                # service is normally active on that day of the week
                row = (serviceid,cd_XmasDate,2)    # Then disable it.
                calendar_dates.writerow(row)       # And write that row.
                print '                 ',row      # And print it.

    # New Year's Eve
    NYEDate = datetime.date(thisyear,12,31)
    NYEWD_ISO = NYEDate.isoweekday() # ISO 8601 weekday, where Monday is 1 and Sunday is 7.
    NYEWD_Name = convertDomainTime(NYEWD_ISO, conversiontype='getdomainday') # Convert weekday ISO number to name.
    cd_NYEDate = str(thisyear)+'1231'
    if NYEWD_ISO not in [5,6]:
        # New Year's Eve is not a Friday or a Saturday.
        print "New Year's Eve: on a "+NYEWD_Name+"; writing exceptions..."
        for serviceid in servicedays:
            if 'yellow' in serviceid.lower() or 'turquoise' in serviceid.lower() or 'green' in serviceid.lower():
                # Route is GT-US, RS-DP or WP-AM.
                if 'saturday' in serviceid.lower():
                    # Service is a Saturday service; turn it on:
                    row = (serviceid,cd_NYEDate,1)
                    calendar_dates.writerow(row)
                    print '                 ',row
                else:
                    # Weekday or Sunday service; turn it off:
                    row = (serviceid,cd_NYEDate,2)
                    calendar_dates.writerow(row)
                    print '                 ',row
    else:
        # New Year's Eve is a Friday or a Saturday; do nothing.
        print "New Year's Eve: on a "+NYEWD_Name+"; no service exceptions written."

    # NATIONALS HOME STADIUM DATES
    with open(os.path.join(scriptloc, 'natsschedule.csv'), 'rb') as file:
        reader = csv.DictReader(file, delimiter=',')#read the csv file
        for row in reader:
            gamestart = datetime.datetime.strptime(row['START DATE'] + " " + row['START TIME'], '%m/%d/%y %I:%M %p') # 03/31/17 04:05 PM
            GAME_ISO = gamestart.isoweekday() # ISO 8601 weekday, where Monday is 1 and Sunday is 7.
            #GAME_Name = convertDomainTime(GAME_ISO, conversiontype='getdomainday') # Convert weekday ISO number to name.

            if gamestart.month >= 10:
                #set season
                season = 'Winter'
            else:
                season = 'Summer'
            if row['LOCATION'] == 'Nationals Park - Washington':
                if GAME_ISO in [7]:
                    # Game is a Sunday.
                    # Turn on nats sunday service
                    calendar_dates.writerow(("Blue_Nats-Sun_Sunday_Sunday",gamestart.strftime("%Y%m%d"),1))
                else:
                    # Game is Mon-Sat
                    # Check to see if game starts on or after 6pm
                    if gamestart.hour >= 18:
                        #turn on Nats-Night service
                        calendar_dates.writerow(("Blue_Nats_Monday_Saturday",gamestart.strftime("%Y%m%d"),1))

    gtfs.writestr('calendar_dates.txt', calendar_dates_file.getvalue())
    print 'Wrote calendar_dates.txt.'

    # Build directional route stop list
    directionalroutestoplist = dict()
    with arcpy.da.SearchCursor(stopevents, ['route', 'MEAS', 'STOPORDER', 'INPUTOID', 'STOPID'],
                               sql_clause=(None, 'ORDER BY route, STOPORDER')) as cursor:
        for stopevent in cursor:
            line, direction = stopevent[0].split("_To")

            if line in directionalroutestoplist:
                if direction in directionalroutestoplist[line]:
                    # Add new stop event to existing direction.
                    directionalroutestoplist[line][direction][stopevent[2]] = stopevent[4]
                else:
                    # Add new direction to existing line.
                    directionalroutestoplist[line][direction] = {stopevent[2]:stopevent[4]}
                    directionalroutestoplist[line][direction][-1] = 1 # negative 1 indicates the direction id.

            else:
                # Create new line, direction and first stop event.
                directionalroutestoplist[line] = {direction: {stopevent[2]: stopevent[4]}, 'route': line}
                directionalroutestoplist[line][direction][-1] = 0 # negative 1 indicates the direction id.

    # Hard-code agency.txt file.

    agency_file = StringIO()
    agency = csv.writer(agency_file)
    agency.writerow(['agency_id', 'agency_name', 'agency_url', 'agency_timezone', 'agency_phone', 'agency_lang']) # agency file header
    agency.writerow(['dc-circulator', 'DC Circulator', 'http://www.dccirculator.com', 'America/New_York', '(202) 962-1423', 'en'])

    gtfs.writestr('agency.txt',agency_file.getvalue())
    print 'Wrote agency.txt.'

    # Hard-code fare_attributes.txt file.

    fare_attributes_file = StringIO()
    fare_attributes = csv.writer(fare_attributes_file)
    fare_attributes.writerow(['fare_id', 'price', 'currency_type', 'payment_method', 'transfers', 'transfer_duration']) # agency file header
    fare_attributes.writerow(['Full-SmarTrip', '1.00', 'USD', '0', '','7200'])

    gtfs.writestr('fare_attributes.txt',fare_attributes_file.getvalue())
    print 'Wrote fare_attributes.txt.'

    stops_file = StringIO()
    stops = csv.writer(stops_file)
    stops.writerow(['stop_id', 'stop_code', 'stop_name', 'stop_lat', 'stop_lon', 'wheelchair_boarding']) # stops file header

    where_clause = "StopStatus = 'Active'"
    with arcpy.da.SearchCursor(WGSstops, ['OID@', 'LINE', 'STOP', 'LineID', 'StopStatus', 'SHAPE@XY', 'ADACOMPLIANT', 'REG_ID'],
                               where_clause=where_clause) as cursor:
        for stop in cursor:
            point = stop[5]

            thisid = stop[7]

            if thisid and thisid != '' and thisid != ' ':
                stopid = thisid #regional bus id
            else:
                stopid = str(stop[0]) + 'DDOT' #no regional bus id

            if stop[6] == 1:
                # ADA Compliant
                wheelchair_boarding = 1
            elif stop[6] ==2:
                # Not ADA Compliant
                wheelchair_boarding = 2
            else:
                # ADA Compliance Unknown
                wheelchair_boarding = 0

            row = (stopid, stop[7], stop[2], point[1], point[0], wheelchair_boarding)
            stops.writerow(row)

    # Write the stops file.
    gtfs.writestr('stops.txt', stops_file.getvalue())
    print 'Wrote stops.txt.'

    shapes_file = StringIO()
    shapes = csv.writer(shapes_file)
    shapes.writerow(['shape_id', 'shape_pt_lat', 'shape_pt_lon', 'shape_pt_sequence'])

    where_clause = "LINE_STATUS = 'Active'"
    with arcpy.da.SearchCursor(WGSlines, ['RouteID', 'LINE_STATUS', 'SHAPE@'], where_clause=where_clause) as cursor:
        for shape in cursor:
            thissequence = 0
            print("Feature {0}:".format(shape[0]))
            partnum = 0

            for part in shape[2]:

                print("Part {0}:".format(partnum))

                for pnt in part:
                    if pnt:
                        print("{0}, {1}".format(pnt.X, pnt.Y))
                        shaperow = (shape[0], pnt.Y, pnt.X, thissequence)
                        shapes.writerow(shaperow)
                    else:
                        print("Interior Ring:")
                    thissequence += 1


    # Write the shapes info
    gtfs.writestr('shapes.txt', shapes_file.getvalue())
    print 'Wrote shapes.txt.'

    routes_file = StringIO()
    routes = csv.writer(routes_file)
    header = ('route_id', 'route_short_name', 'route_long_name', 'route_type', 'route_color', 'route_text_color') # routes file header
    routes.writerow(header)

    trips_file = StringIO()
    trips = csv.writer(trips_file)
    header = ('route_id', 'service_id', 'trip_id', 'trip_headsign', 'shape_id', 'direction_id', 'wheelchair_accessible', 'bikes_allowed') # trips file header
    trips.writerow(header)

    stop_times_file = StringIO()
    stop_times = csv.writer(stop_times_file)
    header = ('trip_id', 'arrival_time', 'departure_time', 'stop_id', 'stop_sequence', 'pickup_type','drop_off_type') # stop_times file header
    stop_times.writerow(header)

    frequencies_file = StringIO()
    frequencies = csv.writer(frequencies_file)
    header = ('trip_id', 'start_time', 'end_time', 'headway_secs', 'exact_times') #frequencies file header
    frequencies.writerow(header)

    # Build Routes.  Store in route dict.

    myroutes = dict()
    with arcpy.da.SearchCursor(localroutes, ['OID@', 'ROUTEID'], sql_clause=(None, 'ORDER BY ROUTEID')) as cursor:
        for thisroute in cursor:
            thisline = thisroute[1].split("_")[0]
            thisdirection = thisroute[1].split("_")[1]
            thisdirection.replace("To ", "")

            # Store info for routes...
            if thisline not in myroutes:
                # Add the line.
                myroutes[thisline] = {'ROUTEID': thisroute[0], 'ID': thisroute[0], 'Direction1': thisdirection,
                                      'LINE': thisroute[1]}
            else:
                # thisline exists.  Add to existing route.
                myroutes[thisline]['Direction2'] = thisdirection


    route_colors = {'red': 'ED1C2F',
                    'blue': '2C5DAB',
                    'turquoise': '00AEEF',
                    'orange': 'F58220',
                    'yellow': 'FDB913',
                    'green': '41AD49'
                    }

    route_text_colors = {'red': 'FFFFFF',
                    'blue': 'FFFFFF',
                    'turquoise': '000000',
                    'orange': '000000',
                    'yellow': '000000',
                    'green': 'FFFFFF'
                    }
    
    route_short_names = {'red': 'NM',
                    'blue': 'US-NY',
                    'turquoise': 'RS-DP',
                    'orange': 'PS',
                    'yellow': 'GT-US',
                    'green': 'WP-AM'
                    }
    
    route_long_names = {
        "yellow": "Georgetown - Union Station",
        "green": "Woodley Park - Adams Morgan - McPherson Square Metro",
        "blue": "Union Station - Navy Yard Metro",
        "turquoise":"Dupont Circle - Georgetown - Rosslyn",
        "orange": "Potomac Ave Metro - Skyland via Barracks Row",
        "red": "National Mall"
    } 
    
    for k, v in myroutes.items():
#        routeshort = v['Direction1'].split(' ')[1] + "-" + v['Direction2'].split(' ')[1]
#        routeshort_fixed = routeshort.replace("To ", "")
        routeshort = route_short_names[k.lower()]
        routecolor = route_colors[k.lower().split("-")[0]]
        routetextcolor = route_text_colors[k.lower().split("-")[0]]
        routelong = route_long_names.get(k.lower())
        routeid = k
        routes.writerow((routeid, routeshort, routelong, 3, routecolor, routetextcolor))

    arcpy.MakeFeatureLayer_management(locallines, "lineslayer",where_clause = "LINE_STATUS = 'Active'")

    # Build Stop_Times and trips:
    trip_id = 0

    daylookup = [d.codedValues for d in arcpy.da.ListDomains(localdb) if d.name == 'LZ_ServiceDay'][0]
    timelookup = [d.codedValues for d in arcpy.da.ListDomains(localdb) if d.name == 'LZ_ServiceHours'][0]

    stopdistance_dict = Table2Dict(stopevents,'ROUTESTOPID',['MEAS'], filter=None)

    # Go Route By Route
    # Using directional stop list, which is structured like this:
    # {red:{'To Union Station': {stoporder:stop_id,...}, 'To Georgetown': {stoporder:stop_id,...}}}
    for line, directiondict in directionalroutestoplist.items():
        # For this line, run a series of trips from the start time until the end time            #
        # down one direction, then the opposing direction, repeat until we reach the end time.   #
        # Also give us a breakdown based on the service days/times available                     #

        # servicegroupdata[thisserviceid] = {"daystart":startid,"dayend":endid,"timestart":timestartid,"timeend":timeendid}
        # Get associated line and day/time info.
        for service_id,service_info in servicegroupdata.items():
            directiontime_dict = dict() # Reset the current route data.
            if line == service_id.split("_")[0]:
                # Make sure we have the appropriate service group, then begin:

                servicehourstart = service_info["timestart"]
                servicehourend = service_info["timeend"]
                servicedaystart = service_info["daystart"]
                servicedayend = service_info["dayend"]

                currenttime = convertDomainTime(17, conversiontype="getdomaindatetime") #start/current doesn't matter since we're using headways.
                starttime = convertDomainTime(servicehourstart,conversiontype="getdomaindatetime")  # reset the starttime and move to next line
                endtime = convertDomainTime(servicehourend,conversiontype="getdomaindatetime")
                starttime_str = convertDomainTime(servicehourstart,conversiontype="getgtfstimestring")  # reset the starttime and move to next line
                endtime_str = convertDomainTime(servicehourend,conversiontype="getgtfstimestring")

                thistriproute = ''

                direction_id = 0
                for direction, stopdict in directiondict.items():

                    if direction == 'route':
                        continue  # This is the route info and it's not the stop order detail.
                    else:
                        thistripdirection = directiondict['route'] + "_To" + direction

                    if thistripdirection not in directiontime_dict:
                        # Initialize the directiontime_dict value with the starting time.
                        directiontime_dict[thistripdirection] = currenttime
                    else:
                        previoustime = directiontime_dict[thistripdirection]
                        previoustime += datetime.timedelta(minutes=interval+10)
                        currenttime = previoustime
                        directiontime_dict[thistripdirection] = currenttime

                    trip_id += 1

                    # header = ('trip_id', 'start_time', 'end_time', 'headway_secs')
                    f_direction = (trip_id,starttime_str,endtime_str,600,1)
                    frequencies.writerow(f_direction)

                    print 'Wrote frequencies file line:', f_direction

                    # End writing frequency row pair.

                    print "Trip: " + str(trip_id)
                    # header = ('route_id', 'service_id', 'trip_id', 'trip_headsign', 'shape_id', 'direction_id', 'wheelchair_accessible', 'bikes_allowed')
                    trips.writerow((line, service_id, trip_id, direction.strip(), thistripdirection, str(stopdict[-1]),1,1))

                    orderedstops = collections.OrderedDict(sorted(stopdict.items()))
                    print 'Line: ' + line
                    print 'Direction: ' + direction
                    prevmeasure = 0

                    for stop_sequence, stop_id in orderedstops.items():
                        if stop_sequence < 0: # This is the direction id value, not an actual stop id.
                            continue

                        print 'working on... ' + thistripdirection + ", Stop ID " + str(stop_id)
                        thismeasure = stopdistance_dict[thistripdirection + "_" + str(stop_id)]['MEAS']
                        # Find interval based on difference of distance.
                        interval = getMinutesFromDistance(thismeasure,prevmeasure)
                        ###HEADWAYS - flat estimate, since we aren't concerned with time estimates###
                        currenttime += datetime.timedelta(minutes=interval)
                        arrivaltime = currenttime.strftime("%H:%M:%S")
                        currenttime += datetime.timedelta(seconds=10)
                        departuretime = currenttime.strftime("%H:%M:%S")
                        stop_times.writerow((trip_id, arrivaltime, departuretime, stop_id, stop_sequence, 0, 0))
                        currenttime += datetime.timedelta(seconds=5)
                        prevmeasure = thismeasure

    gtfs.writestr('routes.txt', routes_file.getvalue())
    print 'Wrote routes.txt.'
    gtfs.writestr('trips.txt', trips_file.getvalue())
    print 'Wrote trips.txt.'
    gtfs.writestr('stop_times.txt', stop_times_file.getvalue())
    print 'Wrote stop_times.txt.'
    gtfs.writestr('frequencies.txt', frequencies_file.getvalue())
    print 'Wrote frequencies.txt.'
    gtfs.close()
    print 'Whoa! GTFS zip archive written!'

    newgtfslocation = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\dc-circulator.zip"
    shutil.copyfile(os.path.join(scriptloc, 'dc-circulator.zip'), newgtfslocation)
    print 'Whoa! GTFS zip archive written to the web location as dc-circulator.zip'
    print 'Terminated at '+datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"."
    
if __name__ == '__main__':
    main()